# Small Python script to convert any video files with subtitle(s) to video supported by Google Chromecast.
#
# Copyright 2021 Arnaud Moura <arnaudmoura@gmail.com>
# This code is released under the terms of the MIT license. See the LICENSE
# file for more details.

# !/usr/bin/env python
# -*- coding: utf-8 -*-

# System
import re
import os
import subprocess
import platform

# Hardware codec supported
nvidia_video_codec_supported = 'h264_nvenc'
cuda_env_name_windows = "CUDA_PATH"
cuda_env_name_linux = "CUDA_PATH"


class Capability:
    def __init__(self):
        # Get platform name
        Capability.platform_name = platform.system()
        
        # Get ffmpeg and ffprobe file name
        Capability.ffmpeg_exe = "ffmpeg.exe"
        Capability.ffprobe_exe = "ffprobe.exe"
        if Capability.platform_name == "Linux":
            Capability.ffmpeg_exe = "ffmpeg"
            Capability.ffprobe_exe = "ffprobe"

        Capability.codec = "libx264"
        Capability.hwaccel_cmd = ""

        re_nvidia_hard = re.compile(r'.*h264_nvenc.*', re.U)

        has_cuda = False
        if ( Capability.platform_name == "Linux" and os.getenv(cuda_env_name_linux) ) or ( Capability.platform_name == "Windows" and os.getenv(cuda_env_name_windows) ):
            has_cuda = True

        pipe = subprocess.Popen(Capability.ffmpeg_exe + ' -encoders', shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines = True)

        for line in pipe.stdout:
            nvidia_hard = re_nvidia_hard.search(line)
            if nvidia_hard and has_cuda:
                Capability.codec = "h264_nvenc"
                Capability.hwaccel_cmd = "-hwaccel cuvid"
                break